//
//  ViewController.swift
//  CollectionViewFillRandomCells
//
//  Created by Sergio Andres Rodriguez Castillo on 03/01/24.
//

import UIKit

class ViewController: UIViewController {
    
    var cellStates: [[Bool]] = []
    let numberOfRows = 5
    let numberOfColumns = 5

    lazy private var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 8
        layout.minimumInteritemSpacing = 8
        
        let collectionView = UICollectionView(frame: view.frame, collectionViewLayout: layout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(CellCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        return collectionView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        cellStates = generateRandomGrid()
        view.addSubview(collectionView)
        self.setUpConstraints()
    }
    
    func generateRandomGrid() -> [[Bool]] {
        var randomGrid: [[Bool]] = []
        
        for _ in 0..<numberOfRows {
            let row = (0..<numberOfColumns).map { _ in Bool.random() }
            randomGrid.append(row)
        }
        
        return randomGrid
    }

    func setUpConstraints() {
        let collectionViewConstraints = [
            collectionView.topAnchor.constraint(equalTo: view.topAnchor),
            collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ]
        NSLayoutConstraint.activate(collectionViewConstraints)
    }
    
    func updateSurroundingCells(at indexPath: IndexPath) {
        let directions: [(Int, Int)] = [
            (0, 1), (1, 0), (0, -1), (-1, 0),   // Horizontal and vertical directions
            (1, 1), (-1, -1), (1, -1), (-1, 1)  // Diagonal directions
        ]
        
        var cellsToUpdate: [IndexPath] = []
        
        for direction in directions {
            let newRow = indexPath.section + direction.0
            let newColumn = indexPath.row + direction.1
            
            if newRow >= 0 && newRow < numberOfRows && newColumn >= 0 && newColumn < numberOfColumns {

                cellStates[newRow][newColumn] = true
                cellsToUpdate.append(IndexPath(row: newColumn, section: newRow))
            }
        }
        
        collectionView.reloadItems(at: [indexPath] + cellsToUpdate)
        
        for cellToUpdate in cellsToUpdate {
            if !cellStates[cellToUpdate.section][cellToUpdate.row] {
                updateSurroundingCells(at: cellToUpdate)
            }
        }
    }
}

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return numberOfRows
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return numberOfColumns
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? CellCollectionViewCell else { return UICollectionViewCell() }
        cell.isBlack = cellStates[indexPath.section][indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let tappedCellIsWhite = !cellStates[indexPath.section][indexPath.row]

        if tappedCellIsWhite {
            cellStates[indexPath.section][indexPath.row] = !cellStates[indexPath.section][indexPath.row]
            updateSurroundingCells(at: indexPath)
            collectionView.reloadItems(at: [indexPath])
        }
    }
}

class CellCollectionViewCell: UICollectionViewCell {
    var isBlack: Bool = false {
        didSet {
            backgroundColor = isBlack ? .black : .white
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.borderWidth = 1.0
        layer.borderColor = UIColor.gray.cgColor
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
